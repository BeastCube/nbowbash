package com.ndependend.bowbash;

import com.ndependend.bowbash.kits.Bogenschuetze;
import com.ndependend.nminigameapi.arena.Arena;
import com.ndependend.nminigameapi.kits.KitChooser;
import com.ndependend.nminigameapi.kits.KitManager;
import com.ndependend.nminigameapi.maps.MapChooser;
import com.ndependend.nminigameapi.nMinigame;
import com.ndependend.nminigameapi.scoreboard.ScoreBoardManager;
import com.ndependend.nminigameapi.scoreboard.ScoreBoardMode;
import com.ndependend.nminigameapi.team.Team;
import com.ndependend.nminigameapi.util.IO;
import com.ndependend.nserverapi.menus.MenuListener;
import com.ndependend.nserverapi.players.Coins;
import com.ndependend.nserverapicommons.minigames.MinigameState;
import com.ndependend.nserverapicommons.minigames.MinigameType;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * @author nDependend
 */
public class BowBash extends nMinigame {

    public static BowBash instance;
    public static final Material GLASS_MATERIAL = Material.STAINED_GLASS;
    public static final HashMap<UUID, UUID> LastDamager = new HashMap<UUID, UUID>();
    public static final HashMap<UUID, Boolean> Flying = new HashMap<UUID, Boolean>();

    @Override
    public void onEnable() {
        instance = this;
        MenuListener.getInstance().register(this);
        registerKits();
        registerStats();
        super.onEnable();
    }

    private static void registerKits() {
        KitManager.registerKit(new Bogenschuetze(0));
    }

    private static void registerStats() {

    }

    @Override
    public MinigameType getMinigameType() {
        return MinigameType.BOWBASH;
    }

    @Override
    public ScoreBoardMode getScoreBoardMode() {
        return ScoreBoardMode.JUST_TEAMS;
    }

    @Override
    public int getMinPlayers() {
        return getNumberOfTeams();
    }

    final int PLAYERS_PER_TEAM = 4;
    @Override
    public int getMaxPlayers() {
        return getNumberOfTeams() * PLAYERS_PER_TEAM;
    }

    @Override
    public int getIngameCountdown() {
        return 7200;
    }

    @Override
    public boolean mayBreakBlock(Block block, Player p) {
        return block.getType() == GLASS_MATERIAL;
    }

    @Override
    public boolean mayPlaceBlock(Block block, Player p) {
        return block.getType() == GLASS_MATERIAL;
    }

    @Override
    public boolean mayGetHungry(Player p) {
        return false;
    }

    @Override
    public void onPlayerDied(Player p) {
        p.sendMessage(ChatColor.RED + "Ihr habt verloren!");
    }

    @Override
    public Arena loadArenaFromConfig() {
        IO.loadWorld(selectedArena, getConfig().getString("worlds"));
        return BowBashArena.fromConfig(getArenaConfig());
    }

    private void spawnGlass() {
        Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
            @Override
            public void run() {
                BowBashArena a = (BowBashArena)BowBash.instance.getArena();
                for(Location l : a.getGlassSpawners()) {
                    l.getWorld().dropItemNaturally(l, new ItemStack(GLASS_MATERIAL, 1, DyeColor.BLUE.getWoolData()));
                }
                spawnGlass();
            }
        }, 30);
    }
    @Override
    public void onGameStarted() {
        for(Team t : currentArena.getTeams()) {
            t.setTeamScore(20);
        }
        spawnGlass();
    }
    @Override
    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent e) {
        super.onPlayerJoin(e);
        if(State.isLobby()) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
                @Override
                public void run() {
                    e.getPlayer().getInventory().setItem(1, MapChooser.MAP_CHOOSER.createItemStack());
                    e.getPlayer().getInventory().setItem(1, KitChooser.KIT_CHOOSER.createItemStack());
                }
            }, 2);
        }
    }
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        if(e.getAction() == Action.LEFT_CLICK_BLOCK) {
            if(e.getClickedBlock().getType() == Material.STAINED_GLASS) {
                e.getPlayer().getInventory().addItem(new ItemStack(Material.STAINED_GLASS, 1, e.getClickedBlock().getData()));
                e.getClickedBlock().setType(Material.AIR);
            }
        }
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        if(State == MinigameState.INGAME) {
            Player p = e.getPlayer();
            p.sendMessage("You are flying: "+Flying.get(p.getUniqueId()));
            //Kill if outside
            if(!currentArena.getBounds().isInside(p.getLocation().toVector())) {
                Team team = getPlayersTeam(p);
                if(team != null) {
                    Player killer = Bukkit.getPlayer(LastDamager.get(p.getUniqueId()));
                    if (killer != null) {
                        Team pt = getPlayersTeam(killer);
                        if (pt != null) {
                            Bukkit.broadcastMessage(pt.getColor() + killer.getName() + ChatColor.GRAY + " hat " + team.getColor() + p.getName() + ChatColor.GRAY + " umgebracht!");
                            pt.setTeamScore(pt.getTeamScore() + 1);
                        }
                        killer.sendMessage(ChatColor.GREEN + "Du hast " + 4 + " Coins erhalten!");
                        Coins.addCoins(killer.getUniqueId(), 4);
                    } else {
                        Bukkit.broadcastMessage(team.getColor() + p.getName() + ChatColor.GRAY + " ist gestorben!");
                    }
                    p.teleport(currentArena.getSpawn(p));
                    if(team.isAlive()) {
                        int teamscore = team.getTeamScore() - 1;
                        team.setTeamScore(teamscore);

                        if(teamscore <= 0) {
                            for(Player tp : team.getPlayers()) {
                                setDead(tp);
                            }
                            List<Team> teamsalive = getTeamsAlive();
                            if (teamsalive.size() == 1) {
                                EndGame(teamsalive.get(0));
                            } else if(teamsalive.size() < 1) {
                                EndGame((Team)null);
                            }
                        }
                    }
                    ScoreBoardManager.updateSB();
                }
            } else {
                if(p.getWorld().getBlockAt(p.getLocation().add(0, -1, 0)).getType() != Material.AIR) {
                    Flying.put(p.getUniqueId(), false);
                }
                launchBlocks(p, e);
            }
        }
    }

    private Vector previousVelocity;
    private double reqpower = 0.1D;
    private double reqstrength = 0.1D;
    private double xzrad = 4;
    private double yrad = 4;
    private double multiplikator = 2;

    private String serializeVector(Vector v) {
        return v == null?"null":"(x = " + Math.round(v.getX() * 100) / 100D + ";y = " +  Math.round(v.getY() * 100) / 100D + ";z = " +  Math.round(v.getZ() * 100) / 100D + ")";
    }

    @EventHandler
    public void onEntityBlockForm(EntityChangeBlockEvent e) {
        if(e.getEntityType() == EntityType.FALLING_BLOCK && e.getTo() == Material.STAINED_GLASS)
            e.setCancelled(true);
    }
    private void launch(Block block, Block headblock, Player p, Vector nextLocation, Vector velocity) {
        double strength = previousVelocity.getX()*previousVelocity.getX() + previousVelocity.getZ()*previousVelocity.getZ();
        Bukkit.broadcastMessage("strength: " + Math.round(strength * 100) / 100D);
        if(strength > reqstrength) {
            Vector bottomleft = new Vector(nextLocation.getX(), nextLocation.getY(), nextLocation.getZ());
            bottomleft.add(new Vector(-Math.abs(previousVelocity.getZ()) * xzrad, -strength * yrad, -Math.abs(previousVelocity.getX() * xzrad)));
            Vector topright = new Vector(nextLocation.getX(), nextLocation.getY(), nextLocation.getZ());
            topright.add(new Vector(Math.abs(previousVelocity.getZ()) * xzrad, strength * yrad + 1, Math.abs(previousVelocity.getX() * xzrad)));

            Bukkit.broadcastMessage("bottomleft: " + serializeVector(bottomleft));
            Bukkit.broadcastMessage("topright: " + serializeVector(topright));

            int xd = topright.getBlockX() - bottomleft.getBlockX();
            int zd = topright.getBlockZ() - bottomleft.getBlockZ();
            if(xd > zd) {
                double zstep = zd / xd;
                double z = bottomleft.getBlockZ();
                for(int x = bottomleft.getBlockX(); x <= topright.getBlockX(); x++) {
                    for(int y = bottomleft.getBlockY(); y <= topright.getBlockY(); y++) {
                        z = z + zstep;
                        launchBlock(p.getWorld().getBlockAt(x, y, (int)z), p, strength);
                    }
                }
            } else {
                double xstep = xd / zd;
                double x = bottomleft.getBlockX();
                for(int z = bottomleft.getBlockX(); z <= topright.getBlockX(); z++) {
                    for(int y = bottomleft.getBlockY(); y <= topright.getBlockY(); y++) {
                        x = x + xstep;
                        launchBlock(p.getWorld().getBlockAt((int)x, y, z), p, strength);
                    }
                }
            }
        }
    }
    private void launchBlock(Block current, Player p, double strength) {
        if(current.getType() == Material.STAINED_GLASS) {
            Material m = current.getType();
            byte data = current.getData();
            current.setType(Material.AIR);
            //spawn falling block
            FallingBlock fallingBlock = current.getWorld().spawnFallingBlock(current.getLocation(), m, data);
            fallingBlock.setVelocity(current.getLocation().toVector().subtract(p.getLocation().toVector()).multiply(strength));
        }
    }
    private void launchBlocks(Player p, PlayerMoveEvent e) {


        Vector velocity = p.getVelocity();
        if(previousVelocity != null) {
            int xp = previousVelocity.getX() > 0?1:-1;
            int zp = previousVelocity.getZ() > 0?1:-1;
            if(previousVelocity.getX()*xp - velocity.getX()*xp + previousVelocity.getZ()*zp - velocity.getZ()*zp > Math.abs(reqpower))
            {
                Vector nextLocation = p.getLocation().toVector().add(previousVelocity.setY(0).multiply(multiplikator));

                Block headblock,block;
                block = p.getWorld().getBlockAt(nextLocation.getBlockX(), nextLocation.getBlockY(), nextLocation.getBlockZ());
                headblock = p.getWorld().getBlockAt(nextLocation.getBlockX(), nextLocation.getBlockY()+1, nextLocation.getBlockZ());
                if(block.getType() == Material.STAINED_GLASS || headblock.getType() == Material.STAINED_GLASS) {
                    launch(block, headblock, p, nextLocation, velocity);
                }
                else
                {
                    nextLocation = p.getLocation().toVector().add(previousVelocity.setY(0).multiply(multiplikator + 1));
                    block = p.getWorld().getBlockAt(nextLocation.getBlockX(), nextLocation.getBlockY(), nextLocation.getBlockZ());
                    headblock = p.getWorld().getBlockAt(nextLocation.getBlockX(), nextLocation.getBlockY()+1, nextLocation.getBlockZ());
                    if(block.getType() == Material.STAINED_GLASS || headblock.getType() == Material.STAINED_GLASS) {
                        launch(block, headblock, p, nextLocation, velocity);
                    }
                }
            }
        }
        previousVelocity = velocity;
    }



    @EventHandler
    public void onEntityDamagedByEntity(EntityDamageByEntityEvent e) {
        super.onEntityDamagedByEntity(e);
        if(State == MinigameState.INGAME) {
            if(e.getDamager() instanceof Player) {
                LastDamager.put(e.getEntity().getUniqueId(), e.getDamager().getUniqueId());
            } else if(e.getDamager() instanceof Arrow) {
                Arrow arrow = (Arrow)e.getDamager();
                arrow.setKnockbackStrength(2);
                ProjectileSource source = arrow.getShooter();
                if(source instanceof Player) {
                    LastDamager.put(e.getEntity().getUniqueId(), ((Player)source).getUniqueId());
                }

                Flying.put(e.getEntity().getUniqueId(), true);
            }
            e.setDamage(0D);
        }
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent e) {
        e.setDamage(0D);
    }

    @SuppressWarnings("unused")
    @EventHandler
    public void onPlayerQuit(final PlayerQuitEvent e) {
        super.onPlayerQuit(e);
        if(State.isInArena()) {
            Team t = getPlayersTeam(e.getPlayer());
            if(t != null) {
                if(!t.isAlive()) {
                    BroadcastTitleMessage(ChatColor.GRAY + "Das Team "+t.getColor()+t.getName()+" ist ausgeschieden!");
                }
            }
            if (getTeamsAlive().size() == 1 && State == MinigameState.INGAME) {
                Team wt = getTeamsAlive().get(0);
                EndGame(wt);
                for(Player wp : wt.getPlayers()) {
                    Coins.addCoins(wp.getUniqueId(), 20);
                    wp.sendMessage(ChatColor.GREEN + "Du hast gewonnen und 20 Coins erhalten!");
                }
            }
        }
    }

    private void cmd_addspawner(CommandSender sender) {
        if (State == MinigameState.DEVELOPMENT) {
            if (sender instanceof Player) {
                Player p = (Player)sender;
                ((BowBashArena)currentArena).getGlassSpawners().add(p.getLocation());
                p.sendMessage(ChatColor.GREEN + "Es wurde erfolgreich ein neuer Spawner hinzugefügt!");
            } else {
                sender.sendMessage(ChatColor.RED + "Nur Spieler können dieses Kommando ausführen!");
            }
        } else {
            sender.sendMessage(ChatColor.RED + "Das Spiel muss im Development-Modus laufen!");
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!sender.hasPermission(cmd.getPermission()))
            sender.sendMessage(ChatColor.RED + "Du bist nicht berechtigt, dieses Kommando auszuführen.");
        else if (cmd.getName().equalsIgnoreCase("addspawner")) {
            cmd_addspawner(sender);
        } else if (cmd.getName().equalsIgnoreCase("setproperty")) {
            if(args.length > 1) {
                if(args[0].equalsIgnoreCase("reqpower")) {
                    reqpower = Double.parseDouble(args[1]);
                }
                else if(args[0].equalsIgnoreCase("reqstrength")) {
                    reqstrength = Double.parseDouble(args[1]);
                }
                else if(args[0].equalsIgnoreCase("xzrad")) {
                    xzrad = Double.parseDouble(args[1]);
                }
                else if(args[0].equalsIgnoreCase("yrad")) {
                    yrad = Double.parseDouble(args[1]);
                }
                else if(args[0].equalsIgnoreCase("multiplikator")) {
                    multiplikator = Double.parseDouble(args[1]);
                }
                else {
                    sender.sendMessage("This property does not exist!");
                    return true;
                }

                sender.sendMessage("Successfully set " + args[0] + " to "+args[1]);
            }
        }
        else
            return false;
        return true;
    }


}
