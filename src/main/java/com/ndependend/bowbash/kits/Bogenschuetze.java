package com.ndependend.bowbash.kits;

import com.ndependend.nminigameapi.kits.ItemStackKit;
import com.ndependend.nminigameapi.util.InventoryCreator;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * @author nDependend
 */
public class Bogenschuetze extends ItemStackKit {

    public static org.bukkit.inventory.Inventory getInventory() {
        Inventory inv = Bukkit.createInventory(null, InventoryType.PLAYER);
        inv.setItem(0, InventoryCreator.CreateItemStack(Material.BOW, 1, (short)0, ChatColor.GRAY + "Bogen", new String[0], new Enchantment[]{
                Enchantment.ARROW_INFINITE
        }, new int[] {
                1
        }));
        inv.setItem(10, new ItemStack(Material.ARROW));
        return inv;
    }

    public Bogenschuetze(int id) {
        super("Bogenschütze", "Bogen, Pfeil, BowBash.", getInventory(), null, null, id, Material.BOW, (short)0);
    }
}
