package com.ndependend.bowbash;

import com.ndependend.nminigameapi.arena.Arena;
import com.ndependend.nminigameapi.arena.Cuboid;
import com.ndependend.nminigameapi.team.Team;
import com.ndependend.nminigameapi.util.Serializer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author nDependend
 */
public class BowBashArena extends Arena {

    ArrayList<Location> GlassSpawner;

    public BowBashArena(ArrayList<Team> teams, Location spectatorSpawn, Cuboid bounds, List<String> builders, ItemStack icon, ArrayList<Location> GlassSpawner) {
        super(teams, spectatorSpawn, bounds, builders, icon);
        this.GlassSpawner = GlassSpawner;
    }

    public ArrayList<Location> getGlassSpawners() {
        return GlassSpawner;
    }

    public static BowBashArena fromConfig(FileConfiguration config) {
        try {
            //Teams(Stringarray):
            //Cuboid:
            //  Vector1:
            //  Vector2:
            //---------------Teams-----------------------------//
            List<String> steams = config.getStringList("Teams");
            ArrayList<Team> teams = new ArrayList<Team>();
            for(String t : steams) {
                teams.add(Serializer.deserializeTeam(t));
            }
            //---------------SpectatorSpawn--------------------//
            String s = config.getString("SpectatorSpawn");
            Location spectator;
            if(s != null)
                spectator = Serializer.deserializeLocation(s);
            else
                spectator = Bukkit.getWorld(BowBash.instance.getSelectedArena()).getSpawnLocation();
            //---------------Spawner---------------------------//
            steams = config.getStringList("Spawner");
            ArrayList<Location> spawner = new ArrayList<Location>();
            for(String l : steams) {
                spawner.add(Serializer.deserializeLocation(l));
            }
            //---------------Cuboid----------------------------//
            Vector vector1 = config.getVector("Cuboid.Vector1");
            Vector vector2 = config.getVector("Cuboid.Vector2");
            Cuboid cuboid;
            if(vector1 == null || vector2 == null)
                cuboid = new Cuboid(new Vector(0,0,0), new Vector(0,0,0));
            else
                cuboid = new Cuboid(vector1, vector2);
            //---------------Icon, builders-------------------//
            ItemStack icon = config.getItemStack("Icon");
            if(icon == null) {
                icon = new ItemStack(Material.BARRIER);
            }
            List<String> builders = config.getStringList("Builders");
            if(builders == null)
                builders = new ArrayList<String>();


            return new BowBashArena(teams, spectator, cuboid, builders, icon, spawner);
        } catch (Exception ex) {
            System.out.println("An exception occured whilst loading arena from config: "+ex.getMessage());
            ex.printStackTrace();
            return null;
        }
    }

    public void toConfig(FileConfiguration config, String file) {
        System.out.println("Saving arena...");

        ArrayList<String> steams = new ArrayList<String>();
        for(Team t : Teams) {
            steams.add(Serializer.serializeTeam(t));
        }
        config.set("Teams", steams);

        config.set("SpectatorSpawn", Serializer.serializeLocation(SpectatorSpawn));

        ArrayList<String> slocs = new ArrayList<String>();
        for(Location l : GlassSpawner)
            slocs.add(Serializer.serializeLocation(l));
        config.set("Spawner", slocs);

        config.set("Cuboid.Vector1", Bounds.V1);
        config.set("Cuboid.Vector2", Bounds.V2);

        config.set("Icon", getIcon());
        config.set("Builders", getBuilders());

        try {
            System.out.println("Saving arena config to "+file);
            config.save(file);
        } catch (IOException e) {
            Bukkit.broadcastMessage(ChatColor.RED + "Die Arena konnte nicht gespeichert werden!");
        }
    }


}
